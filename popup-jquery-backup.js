//record all steps performed
var steps = null;
var stepNo = 0;
var guid = null;

chrome.browserAction.onClicked.addListener(function (tab) {
  if(guid !== null){
    //sync extension state from storage
    chrome.storage.sync.get(guid, function(value) {
      // Notify that we saved.
      console.log("Value retrieved from storage for key ("+ guid +") - " + JSON.stringify(value));
      printRecordedSteps(value.guid.steps);
    });
  }
});

//listen to button click events
$(document).on('click', 'button', function(event){
    event.stopPropagation();
    var target = event.target;
    //check for BUTTON or LINK
    if(target.tagName === "BUTTON"){
      console.log(target);
      if(target.innerText == 'Start'){
        alert("StartRecording()")
        startRecording();
      }
      else if(target.innerText == 'Stop'){
        alert("StopRecording()")
        stopRecording();
      }
      else if(target.innerText == 'Save'){
        saveRecording();
      }
      else if(target.innerText == 'Restart'){
        restartRecording();
      }
    }
});

//listen to incoming messages from content script
chrome.runtime.onMessage.addListener(
function(request, sender, sendResponse) {
  console.log(sender.tab ?
              "Got Message (" + request.message + ") from a content script:" + sender.tab.url :
              "Got Message (" + request.message + ") from the extension");
  switch(request.message){
    case 'STEP':
      manageStepSequence(request.value);
      break;
  }

});

function sendMessageToScript(msg){
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    console.log(tabs[0].id);
    chrome.tabs.sendMessage(tabs[0].id, msg, function(response) {
      console.log("Extension : Sent Message to Script - " + JSON.stringify(msg));
      console.log("Got Response - " + JSON.stringify(response));
      if(response !== null){
        handleResponseFromScript(response);
      }

    });
 });
}

function handleResponseFromScript(response){
  if(response.id === guid){
    alert("Extension : session ID matches");
    switch(response.message){
      case 'STARTED_RECORDING':
        document.getElementById('content').innerText = "Started Recording..";
        break;
      case 'STOPPED_RECORDING':
        document.getElementById('content').innerText = "Stopped Recording..";
        break;
    }
  }

}

//start recoding user actions
function startRecording(){
  //instantiate step object
  steps = {};
  //generate new GUID to store steps in storage for this session
  guid = generateGUID();
  sendMessageToScript({message: {id: guid, value: "START"}});
  //TODO
  //handleResponseFromScript({message: 'STARTED_RECORDING'});
  //replace button text
  document.getElementById('start').disabled = true;
}

//stop recording user actions
function stopRecording(){
  console.log("called stopRecording()");
  //reset guid for this session
  guid = null;
  sendMessageToScript({message: {id: guid, value: "STOP"}});
  //TODO
  //handleResponseFromScript({message: 'STOPPED_RECORDING'});
  //replace button text
  document.getElementById('start').innerText = 'Restart';
  document.getElementById('start').disabled = false;
  document.getElementById('stop').innerText = 'Save';
  //reset steps object
  steps = {};
}

//save the recorded steps
function saveRecording(){
  console.log("called saveRecording()");
  //TODO
  document.getElementById('content').innerText = "Saving Recording..";
  alert("Saved Recording");
  //TODO save recording
  document.getElementById('start').innerText = 'Start';
  document.getElementById('stop').innerText = 'Stop';
  document.getElementById('content').innerText = "Click below to Start/Stop recording..";


}

function restartRecording(){
  console.log("called restartRecording()");
  sendMessageToScript({message: "START"});
  document.getElementById('start').innerText = 'Start';
  document.getElementById('start').disabled = true;
  document.getElementById('stop').innerText = 'Stop';
}

//maintain sequence of steps and append this sequence in popup.html
function manageStepSequence(step){
  stepNo++;
  steps[stepNo] = step;
  var stepText = 'I';
  switch(step.element){
    case 'SELECT':
      stepText += ' selected ' + step.value + ' option.';
      break;
    case 'CHECKBOX':
      stepText += ' checked ' + step.value + ' checkbox.';
      break;
    case 'RADIO':
      stepText += ' selected ' + step.value + ' radio button.';
      break;
    case 'INPUT':
      stepText += ' typed ' + step.value + ' in input text.';
      break;
    case 'TEXTAREA':
      stepText += ' entered ' + step.value + ' in text area.';
      break;
    case 'BUTTON':
      stepText += ' clicked ' + step.value + ' button.';
      break;
    case 'LINK':
      stepText += ' clicked ' + step.value + ' link.';
      if(guid === null) throw new Error('Guid is null');
      setInStorage({guid: {steps: steps}}); //save current STEPS state in storage
      break;
    case 'URL':
      stepText += ' go to ' + step.value + ' url.';
      console.log("Got URL - " + step.value);
      break;
  }
  //append steps in popup.html
  var h4 = document.createElement('h4');
  var br = document.createElement('br');
  h4.innerText = '('+ stepNo+') ' + stepText;
  document.getElementById('steps').append(h4);
  document.getElementById('steps').append(br);
}

//TODO remove it later. Just for console logging
function printRecordedSteps(steps){
  for(var step in steps){
    if(steps.hasOwnProperty(step)){
      manageStepSequence(steps[step]);
      console.log(step + ':' + JSON.stringify(steps[step]));
    }
  }
}
