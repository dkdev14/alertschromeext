
var state = "";

//check if navigated to new page (and we are still recording)
//and send url to extension if true
getFromStorage('state', function(data){
  if(data.state === 'STARTED_RECORDING'){
    console.log("matches state..." + window.location.href);
    //send current url to extension
    sendMessageToExtension({message: 'STEP', value: {element: 'URL', value: window.location.href}});
    activateEventListeners();
  }
});


//instantiate css selector generator
var selectorGenerator = new CssSelectorGenerator;

//listen to user events from extension
chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    console.log(sender.tab ?
            "Got Message (" + request.message + ") from a content script:" + sender.tab.url :
            "Got Message (" + request.message + ") from the extension");

    if (request.message === "START"){
      activateEventListeners();
      state = "STARTED_RECORDING";
    }
    else if(request.message === "STOP"){
      deactivateEventListeners();
      state = "STOPPED_RECORDING";
    }
    sendResponse({message: state});
    setInStorage({'state': state});
});

//store data in chrome
function setInStorage(obj){
  chrome.storage.sync.set(obj, function() {
    // Notify that we saved.
    console.log("Object saved in storage - " + JSON.stringify(obj))
  });
}

//store data in chrome
function getFromStorage(key, cb){
  chrome.storage.sync.get(key, function(value) {
    // Notify that we saved.
    console.log("Value retrieved from storage for key (" + key + ") - " + JSON.stringify(value));
    cb(value);
  });
}

//send message to extension
function sendMessageToExtension(message){
  chrome.runtime.sendMessage(message, function(response) {
    console.log("Script : Sent Message to Extension - " + message.message);
    console.log("Got Response - " + JSON.stringify(response));
  });
}

//activate event listeners on the user page
function activateEventListeners(){
  handleClickEvents();
  handleOnChangeEvents();
}

//deactivate event listeners on the user page
function deactivateEventListeners(){
  //TODO deactivate exisiting event listeners
}

//listen to user clicks from user page
function handleClickEvents(){
    document.onclick = function(event) {
      event = event || window.event; // IE specials
      var target = event.target || event.srcElement; // IE specials
      var selector = null;

      console.log("Target Outside - " + target.tagName);
      //check for BUTTON or LINK
      while(target.tagName !== 'BODY'){
        if(target.tagName == "BUTTON" || target.tagName == "A"){
            break;
        }
          console.log("Target Inside - " + target.tagName);
          target = target.parentNode;

      }

      //check if target is not undefined
      if(!target){
        console.log("Target is - " + target);
        return null;
      }

      return {
        "element": target.tagName == "BUTTON" ? target.tagName : "LINK",
        "action": "CLICK",
        "selector": selectorGenerator.getSelector(target), //TODO add xpath selector as well
        "value": target.text //TODO
      }

    };
}

//listen to user clicks from user page
function handleOnChangeEvents(){
    var step = null;
    console.log("Activated onchange listener");
    document.onchange = function(event) {
      event = event || window.event; // IE specials
      var target = event.target || event.srcElement; // IE specials
      var selector = null;
      while(target !== null &&
            (target.nodeName !== "SELECT" &&
            target.nodeName !== "INPUT" &&
            target.nodeName !== "TEXTAREA")){
          target = target.parentNode;
      }

      //check if target is not undefined
      if(!target){
        return null;
      }

      switch(target.nodeName){
        case 'SELECT':
          step = handleSelectEvents(target);
          break;
        case 'INPUT':
          step = handleInputEvents(target);
          break;
        case 'TEXTAREA':
          step = handleTextAreaEvents(target);
          break;
      }

      console.log(selectorGenerator.getSelector(target));
      //send message to extension
      sendMessageToExtension({message: 'STEP', value: step})
    }
};


//get step object from SELECT event handler
function handleSelectEvents(target){
  console.log("User has selected - " + target.value);
  var step = {
    "element": "SELECT",
    "action": "SELECT",
    "selector": selectorGenerator.getSelector(target), //TODO add xpath selector as well
    "value": target.options[target.selectedIndex].text
  }
  //send message to extension
  sendMessageToExtension({message: 'STEP', value: step})
}

//get step object from INPUT event handler
function handleInputEvents(target){
  var step = null;
  switch(target.attributes["type"].value){
    case "checkbox":
      console.log("Its a checkbox");
      step = {
        "element": "CHECKBOX",
        "action": "CHECK",
        "selector": selectorGenerator.getSelector(target), //TODO add xpath selector as well
        "value": target.value
      }
      break;
    case "radio":
      console.log("Its a radio");
      step = {
        "element": "RADIO",
        "action": "CHECK",
        "selector": selectorGenerator.getSelector(target), //TODO add xpath selector as well
        "value": target.value
      }
      break;
    default :
      console.log("Its some input");
      step = {
        "element": "INPUT",
        "action": "TYPE",
        "selector": selectorGenerator.getSelector(target), //TODO add xpath selector as well
        "value": target.value
      }
      break;
  }
  //send message to extension
  sendMessageToExtension({message: 'STEP', value: step})
}

//get step object from TEXTAREA event handler
function handleTextAreaEvents(target){
  console.log("Its a textarea");
  var step = {
    "element": "TEXTAREA",
    "action": "TYPE",
    "selector": selectorGenerator.getSelector(target), //TODO add xpath selector as well
    "value": target.value
  }
  //send message to extension
  sendMessageToExtension({message: 'STEP', value: step})
}
