//record all steps performed
var stepNo = 0;
var guid = null;


//listen to button click events
$(document).on('click', 'button', function(event){
    event.stopPropagation();
    var target = event.target;
    //check for BUTTON or LINK
    if(target.tagName === "BUTTON"){
      console.log(target);
      if(target.innerText == 'Start'){
        alert("StartRecording()")
        startRecording();
      }
      else if(target.innerText == 'Stop'){
        alert("StopRecording()")
        stopRecording();
      }
      else if(target.innerText == 'Save'){
        saveRecording();
      }
      else if(target.innerText == 'Restart'){
        restartRecording();
      }
    }
});

//listen to incoming messages from content script
chrome.runtime.onMessage.addListener(
function(request, sender, sendResponse) {
  console.log(sender.tab ?
              "Got Message (" + request.message + ") from a content script:" + sender.tab.url :
              "Got Message (" + request.message + ") from the extension");
  switch(request.message){
    case 'PRINT':
      printRecordedSteps(request.value);
      break;
    case 'APPEND_STEP':
      manageStepSequence(request.value);
      break;
    case 'SCRIPT_STARTED_RECORDING':
      document.getElementById('content').innerText = "Started Recording..";
      break;
    case 'SCRIPT_STOPPED_RECORDING':
      document.getElementById('content').innerText = "Stopped Recording..";
      break;
  }

});

function sendMessageToBackground(msg){
  chrome.runtime.sendMessage(msg, function(response) {});
}

//start recoding user actions
function startRecording(){
  //instantiate step object
  steps = {};
  //generate new GUID to store steps in storage for this session
  sendMessageToBackground({message: "START"});
  //TODO
  //handleResponseFromScript({message: 'STARTED_RECORDING'});
  //replace button text
  document.getElementById('start').disabled = true;
}

//stop recording user actions
function stopRecording(){
  console.log("called stopRecording()");
  sendMessageToBackground({message: "STOP"});
  //TODO
  //handleResponseFromScript({message: 'STOPPED_RECORDING'});
  //replace button text
  document.getElementById('start').innerText = 'Restart';
  document.getElementById('start').disabled = false;
  document.getElementById('stop').innerText = 'Save';
  //reset steps object
  steps = {};
}

//save the recorded steps
function saveRecording(){
  console.log("called saveRecording()");
  //TODO
  document.getElementById('content').innerText = "Saving Recording..";
  alert("Saved Recording");
  //TODO save recording
  document.getElementById('start').innerText = 'Start';
  document.getElementById('stop').innerText = 'Stop';
  document.getElementById('content').innerText = "Click below to Start/Stop recording..";


}

function restartRecording(){
  console.log("called restartRecording()");
  sendMessageToBackground({message: "START"});
  document.getElementById('start').innerText = 'Start';
  document.getElementById('start').disabled = true;
  document.getElementById('stop').innerText = 'Stop';
}

//maintain sequence of steps and append this sequence in popup.html
function manageStepSequence(step){
  stepNo++;
  var stepText = 'I';
  switch(step.element){
    case 'SELECT':
      stepText += ' selected ' + step.value + ' option.';
      break;
    case 'CHECKBOX':
      stepText += ' checked ' + step.value + ' checkbox.';
      break;
    case 'RADIO':
      stepText += ' selected ' + step.value + ' radio button.';
      break;
    case 'INPUT':
      stepText += ' typed ' + step.value + ' in input text.';
      break;
    case 'TEXTAREA':
      stepText += ' entered ' + step.value + ' in text area.';
      break;
    case 'BUTTON':
      stepText += ' clicked ' + step.value + ' button.';
      break;
    case 'LINK':
      stepText += ' clicked ' + step.value + ' link.';
      break;
    case 'URL':
      stepText += ' go to ' + step.value + ' url.';
      break;
  }
  //append steps in popup.html
  var h4 = document.createElement('h4');
  var br = document.createElement('br');
  h4.innerText = '('+ stepNo+') ' + stepText;
  document.getElementById('steps').append(h4);
  document.getElementById('steps').append(br);
}

//TODO remove it later. Just for console logging
function printRecordedSteps(steps){
  for(var step in steps){
    if(steps.hasOwnProperty(step)){
      manageStepSequence(steps[step]);
      console.log(step + ':' + JSON.stringify(steps[step]));
    }
  }
}
