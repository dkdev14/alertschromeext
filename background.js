//record all steps performed
var steps = null;
var stepNo = 0;
var guid = null;
var stepText = null;
var state = null;
var someStorageKey = 'f4vs-vfg5-ymu46-6g5t-43453';


chrome.browserAction.onClicked.addListener(function (tab) {
  alert("Inside browserAction");
  if(guid !== null){
    //sync extension state from storage
    chrome.storage.sync.get(guid, function(value) {
      // Notify that we saved.
      console.log(`Background : Value retrieved from storage for
      key (${guid}) - ` + JSON.stringify(value));
      if(guid !== null){
        alert("sending message to popup to PRINT");
        sendMessageToPopup({message: 'PRINT', value: steps});
      }
    });
  }
});



//listen to incoming messages
chrome.runtime.onMessage.addListener(
function(request, sender, sendResponse) {
  console.log(sender.tab ?
              "Got Message (" + request.message + ") from a content script:" + sender.tab.url :
              "Got Message (" + request.message + ") from the extension");
  switch(request.message){
    case 'STEP':
      alert("Background : got STEP " + request.value.element + " - " + request.value.value);
      stepNo++;
      steps[stepNo] = request.value;
      //get current state of the session
      var storeObj = getCurrentState();
      setInStorage({someStorageKey: storeObj})
      sendMessageToPopup({message: 'APPEND_STEP', value: request.value});
      break;
    case 'START':

      state = 'STARTED_RECORDING';
      //add url to steps
      //stepNo++;
      //steps[stepNo] = {"element": 'URL', "value": "test"};
      alert("test");
      //inform popup to display this step
      //sendMessageToPopup({message: 'APPEND_STEP', value: steps[stepNo]});
      guid = generateGUID();
      //get current state of the session
      var storeObj = getCurrentState();
      setInStorage({someStorageKey: storeObj})
      sendMessageToScript({message : { id: guid, value: 'START'}});
      break;
    case 'STOP':
      sendMessageToScript({message : { id: guid, value: 'STOP'}});
      guid = null;
      state = 'STOPPED_RECORDING';
      stepNo = 0;
      steps = null;
      //get current state of the session
      var storeObj = getCurrentState();
      setInStorage({someStorageKey: storeObj})
      break;

  }

});

function sendMessageToScript(msg){
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    console.log(tabs[0].id);
    chrome.tabs.sendMessage(tabs[0].id, msg, function(response) {
      console.log("Background : Sent Message to Script - " + JSON.stringify(msg));
      console.log("Background : Got Response from Script - " + JSON.stringify(response));
      if(response !== null){
        handleResponseFromScript(response);
      }

    });
 });
}

//send message to popup
function sendMessageToPopup(message){
  chrome.runtime.sendMessage(message, function(response) {});
}

function handleResponseFromScript(response){
    switch(response.message){
      case 'STARTED_RECORDING':
        sendMessageToPopup({message: 'SCRIPT_STARTED_RECORDING'});
        break;
      case 'STOPPED_RECORDING':
        sendMessageToPopup({message: 'SCRIPT_STOPPED_RECORDING'});
        break;
    }
}


//generate GUID for our recording to be uniquely stored in storage
function generateGUID() {
    var S4 = function() {
       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}

//store data in chrome
function setInStorage(obj){
  chrome.storage.sync.set(obj, function() {
    // Notify that we saved.
    console.log("Object saved in storage - " + JSON.stringify(obj))
  });
}

//update current state of session
function getCurrentState(){
  var obj = {};
  obj.id = guid;
  obj.steps = steps;
  obj.state = state;
  return obj;
}
