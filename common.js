//store data in chrome
function setInStorage(obj){
  chrome.storage.sync.set(obj, function() {
    // Notify that we saved.
    console.log("Object saved in storage - " + JSON.stringify(obj))
  });
}



//store data in chrome
function getFromStorage(key, cb){
  chrome.storage.sync.get(key, function(value) {
    // Notify that we saved.
    console.log("Value retrieved from storage for key (" + key + ") - " + JSON.stringify(value));
    cb(value);
  });
}

//check if string is URL
function isUrl(str) {
   var regexp = /^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
   return regexp.test(str);
}
