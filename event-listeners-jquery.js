//instantiate css selector generator
var selectorGenerator = new CssSelectorGenerator;
var state = "";
var guid = null;
var someStorageKey = 'f4vs-vfg5-ymu46-6g5t-43453';


//check if navigated to new page (and we are still recording)
//and send url to extension if true
getFromStorage(someStorageKey, function(data){
  console.log("Content-Script : fetched storage data - " + JSON.stringify(data));
  if(data.someStorageKey
    && data.someStorageKey.state === 'STARTED_RECORDING'
    && data.someStorageKey.id !== null){
    state = 'STARTED_RECORDING';
    guid = data.someStorageKey.id;
    alert("Now its official.. " + guid);
  }
});

//listen to user events from extension
chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    console.log(sender.tab ?
            "Content-Script : Got Message (" + JSON.stringify(request.message) + ") from a content script:" + sender.tab.url :
            "Content-Script : Got Message (" + JSON.stringify(request.message) + ") from the extension");
    alert("Content-Script : " + JSON.stringify(request.message));
    if (request.message.value === "START"){
      //assign unique id for this session
      guid = request.message.id;
      state = "STARTED_RECORDING";
    }
    else if(request.message.value === "STOP"){
      //reset unique id for this session
      guid = request.message.id;
      state = "STOPPED_RECORDING";
    }
    sendResponse({id: guid, message: state});
});


//send message to extension if we are recording
function sendMessageToBackgroundScript(message){
  getFromStorage(someStorageKey, function(data){
    if(data.someStorageKey
      && data.someStorageKey.state === 'STARTED_RECORDING'){
      chrome.runtime.sendMessage(message, function(response) {
        console.log("Script : Sent Message to Extension - " + message.message);
        console.log("Got Response - " + JSON.stringify(response));
      });
    }
  });
}

//activate event listeners on the user page
function activateEventListeners(){
  handleClickEvents();
  handleOnChangeEvents();
}

//deactivate event listeners on the user page
function deactivateEventListeners(){
  //TODO deactivate exisiting event listeners
}

//listen to user clicks from user page
function handleClickEvents(){

    $(document).on('click', 'button', function(event){
        event.stopPropagation();
        alert("Button (" + selectorGenerator.getSelector(event.target)  + ") was clicked.");
        var step = null;
        step = {
          "element": "BUTTON",
          "action": "CLICK",
          "selector": selectorGenerator.getSelector(event.target), //TODO add xpath selector as well
          "value": event.target.text //TODO
        }
        //send message to extension
        sendMessageToBackgroundScript({message: 'STEP', value: step});
    });

    $(document).on('click', 'a', function(event){
        event.stopPropagation();
        alert("Link (" + selectorGenerator.getSelector(event.target)  + ") was clicked.");
        var step = null;
        step = {
          "element": "LINK",
          "action": "CLICK",
          "selector": selectorGenerator.getSelector(event.target), //TODO add xpath selector as well
          "value": event.target.text //TODO
        }
        //send message to extension
        sendMessageToBackgroundScript({message: 'STEP', value: step});
      });

    $(document).on('click', 'div', function(event){
        event.stopPropagation();
        var attrs = event.target.attributes;

        for(var i=0; i < attrs.length; i++){
          /*if(isUrl(attrs[i].value)){
            console.log(attrs[i].name + " - " + attrs[i].value);
          }*/
          if(attrs[i].name === 'data-url'){
            console.log("Yas...found data-url");
            var step = null;
            step = {
              "element": "LINK",
              "action": "CLICK",
              "selector": selectorGenerator.getSelector(event.target), //TODO add xpath selector as well
              "value": event.target.text //TODO
            }
            //send message to extension
            sendMessageToBackgroundScript({message: 'STEP', value: step});
            break;
          }
        }
    });

    $(document).on('click', 'span', function(event){
        event.stopPropagation();
        event.preventDefault();
        var attrs = event.target.attributes;

          for(var i=0; i < attrs.length; i++){
            if(isUrl(attrs[i].value)){
              console.log(attrs[i].name + " - " + attrs[i].value);
              var step = null;
              step = {
                "element": "LINK",
                "action": "CLICK",
                "selector": selectorGenerator.getSelector(event.target), //TODO add xpath selector as well
                "value": event.target.text //TODO
              }
              //send message to extension
              sendMessageToBackgroundScript({message: 'STEP', value: step});
              break;
            }
          }
    });
}

//listen to user clicks from user page
function handleOnChangeEvents(){

      $(document).on('change', 'select', function(event){
          alert("Select (" + selectorGenerator.getSelector(event.target)  + ") was changed.");
          var step = null;
          step = {
            "element": "SELECT",
            "action": "SELECT",
            "selector": selectorGenerator.getSelector(event.target), //TODO add xpath selector as well
            "value": event.target.options[event.target.selectedIndex].text
          }

          //send message to extension
          sendMessageToBackgroundScript({message: 'STEP', value: step});
      });

      $(document).on('change', 'input', function(event){
          alert("Input (" + selectorGenerator.getSelector(event.target)  + ") was changed.");
          var step = null;
          var target = event.target;
          switch(target.attributes["type"].value){
            case "checkbox":
              console.log("Its a checkbox");
              step = {
                "element": "CHECKBOX",
                "action": "CHECK",
                "selector": selectorGenerator.getSelector(target), //TODO add xpath selector as well
                "value": target.value
              }
              break;
            case "radio":
              console.log("Its a radio");
              step = {
                "element": "RADIO",
                "action": "CHECK",
                "selector": selectorGenerator.getSelector(target), //TODO add xpath selector as well
                "value": target.value
              }
              break;
            default :
              console.log("Its some input");
              step = {
                "element": "INPUT",
                "action": "TYPE",
                "selector": selectorGenerator.getSelector(target), //TODO add xpath selector as well
                "value": target.value
              }
              break;
          }
          //send message to extension
          sendMessageToBackgroundScript({message: 'STEP', value: step});
      });

      $(document).on('change', 'textarea', function(event){
          alert("Textarea (" + selectorGenerator.getSelector(event.target)  + ") was changed.");
          var step = null;
          step = {
            "element": "TEXTAREA",
            "action": "TYPE",
            "selector": selectorGenerator.getSelector(target), //TODO add xpath selector as well
            "value": target.value
          }
          //send message to extension
          sendMessageToBackgroundScript({message: 'STEP', value: step});
      });
};

activateEventListeners();
