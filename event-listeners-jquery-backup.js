//instantiate css selector generator
var selectorGenerator = new CssSelectorGenerator;
var state = "";
var guid = null;
var thisStateKey = "vfve-566-fwvc-23242-55234";


//check if navigated to new page (and we are still recording)
//and send url to extension if true
getFromStorage(thisStateKey, function(data){
  console.log("Content-Script : fetched storage data - " + JSON.stringify(data));
  if(data.thisStateKey._state === 'STARTED_RECORDING'
    && data.thisStateKey.id !== null){
    state = 'STARTED_RECORDING';
    guid = data.thisStateKey.id;
    alert(guid);
    console.log("matches state..." + window.location.href);
    //send current url to extension
    sendMessageToExtension({message: 'STEP', value: {element: 'URL', value: window.location.href}});
  }
});

//listen to user events from extension
chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    console.log(sender.tab ?
            "Got Message (" + request.message + ") from a content script:" + sender.tab.url :
            "Got Message (" + request.message + ") from the extension");
    alert("Content-Script : " + JSON.stringify(request.message));
    if (request.message.value === "START"){
      //assign unique id for this session
      guid = request.message.id;
      alert("Content-Script : START - " + guid);
      state = "STARTED_RECORDING";
    }
    else if(request.message.value === "STOP"){
      //reset unique id for this session
      guid = request.message.id;
      alert("Content-Script : STOP - " + guid);
      state = "STOPPED_RECORDING";
    }
    sendResponse({id: guid, message: state});
    setInStorage({thisStateKey: {id: guid, _state: state}});
});


//send message to extension if we are recording
function sendMessageToExtension(message){
  getFromStorage('state', function(data){
    if(data.state === 'STARTED_RECORDING'){
      chrome.runtime.sendMessage(message, function(response) {
        console.log("Script : Sent Message to Extension - " + message.message);
        console.log("Got Response - " + JSON.stringify(response));
      });
    }
  });
}

//activate event listeners on the user page
function activateEventListeners(){
  handleClickEvents();
  handleOnChangeEvents();
}

//deactivate event listeners on the user page
function deactivateEventListeners(){
  //TODO deactivate exisiting event listeners
}

//listen to user clicks from user page
function handleClickEvents(){

    $(document).on('click', 'button', function(event){
        event.stopPropagation();
        alert("Button (" + selectorGenerator.getSelector(event.target)  + ") was clicked.");
        var step = null;
        step = {
          "element": "BUTTON",
          "action": "CLICK",
          "selector": selectorGenerator.getSelector(event.target), //TODO add xpath selector as well
          "value": event.target.text //TODO
        }
        //send message to extension
        sendMessageToExtension({message: 'STEP', value: step});
    });

    $(document).on('click', 'a', function(event){
        event.stopPropagation();
        alert("Link (" + selectorGenerator.getSelector(event.target)  + ") was clicked.");
        var step = null;
        step = {
          "element": "LINK",
          "action": "CLICK",
          "selector": selectorGenerator.getSelector(event.target), //TODO add xpath selector as well
          "value": event.target.text //TODO
        }
        //store state of this script in storage
        setInStorage({thisStateKey: {id: guid, _state: state}}); //save current STEPS state in storage
        //send message to extension
        sendMessageToExtension({message: 'STEP', value: step});
      });

    $(document).on('click', 'div', function(event){
        event.stopPropagation();
        var attrs = event.target.attributes;

        for(var i=0; i < attrs.length; i++){
          /*if(isUrl(attrs[i].value)){
            console.log(attrs[i].name + " - " + attrs[i].value);
          }*/
          if(attrs[i].name === 'data-url'){
            console.log("Yas...found data-url");
            //store state of this script in storage
            setInStorage({thisStateKey: {id: guid, _state: state}});
          }
        }
    });

    $(document).on('click', 'span', function(event){
        event.stopPropagation();
        event.preventDefault();
        var attrs = event.target.attributes;

          for(var i=0; i < attrs.length; i++){
            if(isUrl(attrs[i].value)){
              console.log(attrs[i].name + " - " + attrs[i].value);
              //store state of this script in storage
              setInStorage({thisStateKey: {id: guid, _state: state}});
            }
          }
    });
}

//listen to user clicks from user page
function handleOnChangeEvents(){

      $(document).on('change', 'select', function(event){
          alert("Select (" + selectorGenerator.getSelector(event.target)  + ") was changed.");
          var step = null;
          step = {
            "element": "SELECT",
            "action": "SELECT",
            "selector": selectorGenerator.getSelector(event.target), //TODO add xpath selector as well
            "value": event.target.options[event.target.selectedIndex].text
          }

          //send message to extension
          sendMessageToExtension({message: 'STEP', value: step});
      });

      $(document).on('change', 'input', function(event){
          alert("Input (" + selectorGenerator.getSelector(event.target)  + ") was changed.");
          var step = null;
          var target = event.target;
          switch(target.attributes["type"].value){
            case "checkbox":
              console.log("Its a checkbox");
              step = {
                "element": "CHECKBOX",
                "action": "CHECK",
                "selector": selectorGenerator.getSelector(target), //TODO add xpath selector as well
                "value": target.value
              }
              break;
            case "radio":
              console.log("Its a radio");
              step = {
                "element": "RADIO",
                "action": "CHECK",
                "selector": selectorGenerator.getSelector(target), //TODO add xpath selector as well
                "value": target.value
              }
              break;
            default :
              console.log("Its some input");
              step = {
                "element": "INPUT",
                "action": "TYPE",
                "selector": selectorGenerator.getSelector(target), //TODO add xpath selector as well
                "value": target.value
              }
              break;
          }
          //send message to extension
          sendMessageToExtension({message: 'STEP', value: step});
      });

      $(document).on('change', 'textarea', function(event){
          alert("Textarea (" + selectorGenerator.getSelector(event.target)  + ") was changed.");
          var step = null;
          step = {
            "element": "TEXTAREA",
            "action": "TYPE",
            "selector": selectorGenerator.getSelector(target), //TODO add xpath selector as well
            "value": target.value
          }
          //send message to extension
          sendMessageToExtension({message: 'STEP', value: step});
      });
};

activateEventListeners();
